# Asterisk Packaged By Cloudonix

This is the Asterisk packaging project for Cloudonix. It currently includes builds for Asterisk 16, 17, 18 and 20, for "Enterprise Linux" 7, 8 and 9.

The specific build configuration for each is maintained in its own branch:

- [Asterisk 16](-/tree/asterisk-16)
- [Asterisk 17](-/tree/asterisk-17)
- [Asterisk 18](-/tree/asterisk-18)
- [Asterisk 20](-/tree/asterisk-20)

To use these builds in an Enterprise Linux installation, create a file `/etc/yum.repos.d/cloudonix.repo` with the following content:

```ini
[cloudonix]
name = Cloudonix
baseurl = https://cloudonix-dist.s3.amazonaws.com/repository/asterisk-$asteriskver-el$releasever
enabled = 1
gpgcheck = 0
```

Where `$asteriskver` would be replaced with either `16`, `17`, `18` or `20` depending on which Asterisk version should be installed, and `$releasever` would be either `7`, `8` or `9`.

After adding the repository file, run `yum install asterisk` to install the base package and additionally any other optional plugin that is available - use `yum list --disablerepo=* --enablerepo=cloudonix list asterisk*` to list all the available Asterisk packages.

## Containers

The project also generates a container for Asterisk 18 and 20 that can be useful for running this Asterisk build on non "Enterprise Linux" operating systems, such as Ubuntu. The container is based on RedHat UBI Release 8 and is available [in Docker Hub](https://hub.docker.com/r/cloudonix/asterisk).

Example usage:

```bash
docker run -a stdout -a stderr --rm --name asterisk -v /etc/asterisk:/etc/asterisk \
	-p "5060:5060/udp" cloudonix/asterisk:ubi8
```
