FROM centos:7

RUN yum install -y rpm-build rpmdevtools yum-utils epel-release wget opus-devel
RUN yum install -y spandsp-devel
WORKDIR /root/
ADD asterisk.spec /root/rpmbuild/SPECS/asterisk.spec
ADD asterisk.logrotate patches/* /root/rpmbuild/SOURCES/
RUN spectool -g -R /root/rpmbuild/SPECS/asterisk.spec --define 'with_opus 1'
RUN yum-builddep -y /root/rpmbuild/SPECS/asterisk.spec --define 'with_opus 1'
RUN rpmbuild -ba rpmbuild/SPECS/*.spec --with opus --without ooh323 --without odbc --without pgsql
