DESTDIR :=
SRC_IMAGE :=
DOCKER := $(shell test -x /usr/bin/podman && echo podman || echo docker)

build: asterisk sounds

copy-to-destdir:
	mkdir -p $(DESTDIR)
	$(DOCKER) run -a stdout -a stderr --rm -v ${PWD}/$(DESTDIR)/:/out/ $(SRC_IMAGE) /bin/bash -c '\
		cp -r /root/rpmbuild/*RPMS /out/; \
		chown $(shell id -u) /out/ -R \
	'

asterisk-el7:
	$(DOCKER) build -f el7.Dockerfile -t asterisk-build-el7 .
	[ -n "$(DESTDIR)" ] && $(MAKE) copy-to-destdir DESTDIR=$(DESTDIR) SRC_IMAGE=asterisk-build-el7 || true

asterisk-el8:
	$(DOCKER) build -f el8.Dockerfile -t asterisk-build-el8 .
	[ -n "$(DESTDIR)" ] && $(MAKE) copy-to-destdir DESTDIR=$(DESTDIR) SRC_IMAGE=asterisk-build-el8 || true

asterisk-el9:
	$(DOCKER) build -f el9.Dockerfile -t asterisk-build-el9 .
	[ -n "$(DESTDIR)" ] && $(MAKE) copy-to-destdir DESTDIR=$(DESTDIR) SRC_IMAGE=asterisk-build-el9 || true

asterisk: asterisk-el7 asterisk-el8 asterisk-el9

sounds: sounds-core sounds-extra sounds-moh

sounds-core-el7:
	$(DOCKER) build -t asterisk-sounds-build-el7 -f sounds-core-el7.Dockerfile .

sounds-core-el8:
	$(DOCKER) build -t asterisk-sounds-build-el8 -f sounds-core-el8.Dockerfile .
	
sounds-core-el9:
	$(DOCKER) build -t asterisk-sounds-build-el9 -f sounds-core-el9.Dockerfile .
	
sounds-core: sounds-core-el7 sounds-core-el8 sounds-core-el9

sounds-extra-el7:
	$(DOCKER) build -t asterisk-sounds-build-el7 -f sounds-extra-el7.Dockerfile .

sounds-extra-el8:
	$(DOCKER) build -t asterisk-sounds-build-el8 -f sounds-extra-el8.Dockerfile .

sounds-extra-el9:
	$(DOCKER) build -t asterisk-sounds-build-el9 -f sounds-extra-el9.Dockerfile .

sounds-extra: sounds-extra-el7 sounds-extra-el8 sounds-extra-el9

sounds-moh-el7:
	$(DOCKER) build -t asterisk-sounds-build-el7 -f sounds-moh-el7.Dockerfile .

sounds-moh-el8:
	$(DOCKER) build -t asterisk-sounds-build-el8 -f sounds-moh-el8.Dockerfile .

sounds-moh-el9:
	$(DOCKER) build -t asterisk-sounds-build-el9 -f sounds-moh-el9.Dockerfile .

sounds-moh: sounds-moh-el7 sounds-moh-el8 sounds-moh-el9

ubi8-container: .astversion
	$(DOCKER) build -f containers/ubi8-runtime.Dockerfile --build-arg ASTERISK_VERSION=$(shell cat .astversion)  -t asterisk:latest ./containers

ubi9-container: .astversion
	$(DOCKER) build -f containers/ubi9-runtime.Dockerfile --build-arg ASTERISK_VERSION=$(shell cat .astversion) -t asterisk:latest ./containers

.astversion: 
	$(DOCKER) build -f containers/spec-parser.Dockerfile -t read-spec ./
	$(DOCKER) run -ti --rm read-spec | grep -v warning | head -n1 > .astversion
	$(DOCKER) image rm localhost/read-spec:latest

asterisk-el8-image: asterisk-el8 .astversion
	rm -rf containers/rpmbuild
	$(MAKE) asterisk-el8 DESTDIR=./containers/rpmbuild
	$(eval astversion := $(shell cat .astversion))
	$(DOCKER) build -f containers/ubi8-runtime.Dockerfile \
		--build-arg=ASTERISK_VERSION=$(astversion) \
		--build-arg=INSTALL_FROM=local \
		-t quay.io/cloudonix/asterisk:$(astversion) ./containers
	ASTVER=$(astversion) && $(DOCKER) tag quay.io/cloudonix/asterisk:$(astversion) quay.io/cloudonix/asterisk:$${ASTVER%%.*}


asterisk-el9-image: asterisk-el9 .astversion
	rm -rf containers/rpmbuild
	$(MAKE) asterisk-el9 DESTDIR=./containers/rpmbuild
	$(eval astversion := $(shell cat .astversion))
	$(DOCKER) build -f containers/ubi9-runtime.Dockerfile \
		--build-arg=ASTERISK_VERSION=$(astversion) \
		--build-arg=INSTALL_FROM=local \
		-t quay.io/cloudonix/asterisk:$(astversion) ./containers
	ASTVER=$(astversion) && $(DOCKER) tag quay.io/cloudonix/asterisk:$(astversion) quay.io/cloudonix/asterisk:$${ASTVER%%.*}
