ARG INSTALL_FROM=remote
ARG ASTERISK_VERSION=18
ARG LOCAL_RPMS=rpmbuild

FROM registry.access.redhat.com/ubi8-minimal as install_local
ADD ${LOCAL_RPMS} /rpms
RUN rpm -i https://vault.centos.org/centos/8/AppStream/x86_64/os/Packages/createrepo_c-0.17.2-3.el8.x86_64.rpm https://vault.centos.org/centos/8/AppStream/x86_64/os/Packages/createrepo_c-libs-0.17.2-3.el8.x86_64.rpm https://vault.centos.org/centos/8/AppStream/x86_64/os/Packages/drpm-0.4.1-3.el8.x86_64.rpm
RUN cd /rpms && createrepo .
RUN (echo "[cx-local]"; echo "name=Cloudonix local"; echo "enabled=1"; echo "gpgcheck=0"; echo "cost=1"; echo "baseurl=file:///rpms") > /etc/yum.repos.d/cx-local.repo
ADD cx.repo /etc/yum.repos.d/
RUN mkdir -p /etc/dnf/vars
RUN echo sounds > /etc/dnf/vars/asteriskver

FROM registry.access.redhat.com/ubi8-minimal as install_remote
ADD cx.repo /etc/yum.repos.d/
RUN mkdir -p /etc/dnf/vars
RUN echo $ASTERISK_VERSION | cut -d. -f1 > /etc/dnf/vars/asteriskver

FROM install_${INSTALL_FROM}
ADD ubi8-centos-appstream.repo /etc/yum.repos.d/
RUN rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
RUN microdnf upgrade -y
RUN version=$(microdnf repoquery asterisk | grep asterisk-${ASTERISK_VERSION} | tail -n1 | awk -F- '{print$2}'); \
	[ -n "$version" ] || { echo "Failed to find a required package asterisk-${ASTERISK_VERSION}!"; exit 1; }; \
	microdnf install -y \
	asterisk-${version} asterisk-configs-${version} \
	asterisk-addons-core-${version} asterisk-curl-${version} asterisk-snmp-${version} asterisk-opus-opensource-${version} asterisk-sqlite-${version} \
	asterisk-sounds-core-en-alaw asterisk-sounds-core-en-g722 asterisk-sounds-core-en-gsm asterisk-sounds-core-en-ulaw \
	asterisk-sounds-extra-en-alaw asterisk-sounds-extra-en-g722 asterisk-sounds-extra-en-gsm asterisk-sounds-extra-en-ulaw \
	asterisk-sounds-moh-opsound-alaw asterisk-sounds-moh-opsound-g722 asterisk-sounds-moh-opsound-gsm asterisk-sounds-moh-opsound-ulaw
RUN mkdir -p /var/spool/asterisk/recording && chown asterisk:asterisk /var/spool/asterisk/recording
RUN mkdir -p /var/lib/asterisk/moh && chown asterisk:asterisk /var/lib/asterisk/moh
VOLUME /etc/asterisk
VOLUME /var/spool/asterisk
ENTRYPOINT [ "/usr/sbin/asterisk", "-f", "-U", "asterisk", "-G", "asterisk" ]

