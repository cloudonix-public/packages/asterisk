FROM registry.access.redhat.com/ubi8-minimal
RUN microdnf install rpm-build
WORKDIR /root
ADD asterisk.spec /root/asterisk.spec
CMD [ "rpmspec", "-q", "--queryformat", "%{version}\n", "asterisk.spec" ]