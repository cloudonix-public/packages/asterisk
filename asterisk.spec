###################################################################
#
#  asterisk.spec - used to generate asterisk rpm
#  For more info: http://www.rpm.org/max-rpm/ch-rpm-basics.html
#
#  This spec file uses default directories:
#  /usr/src/redhat/SOURCES - orig source, patches, icons, etc.
#  /usr/src/redhat/SPECS - spec files
#  /usr/src/redhat/BUILD - sources unpacked & built here
#  /usr/src/redhat/RPMS - binary package files
#  /usr/src/redhat/SRPMS - source package files
#
###################################################################
#
#  Global Definitions
#
###################################################################
%define astapi 18
%define aversion %{astapi}.21.0
%define arelease 1
%define actversion %(echo %{aversion}|sed -e "s/-.*$//g")
%define subvers %(echo %{aversion}|awk "/-/"|sed -e "s/^.*-//"|awk '{print "0." $1 "."}')
%define actrelease %(echo %{subvers}%{arelease}|sed -e "s/-/_/g")

%define distname centos
%define distver 1

%define digium_opus_ver 1.3.0

%define logdir /var/log

%define _without_misdn 1
%define _without_resample 1
%define _without_voicemail_splitopts 1
%define _without_voicemail_odbcstorage 1
%define _without_voicemail_imapstorage 1
%define _without_ogg 1

%{?el5:%define _lib lib}

%bcond_with bluetooth
%bcond_with dahdi
%bcond_without ooh323
%bcond_with opus
%bcond_without speex
%bcond_with digium_opus
%bcond_without odbc
%bcond_without mysql
%bcond_without newt
%bcond_without alsa
%bcond_without snmp
%bcond_without pgsql
%bcond_without sqlite ## we can't seem to be able to disable sqlite non-core resources
%bcond_with oss
%bcond_with tds
%bcond_without ilbc
%bcond_with ldap
%bcond_with portaudio
%bcond_with jack
%bcond_with radius
%bcond_with pjsip
%bcond_with malloc_debug
%bcond_with system_pjproject
%bcond_without apidocs
%bcond_without verbose

%define __without() %{expand:%%{?with_${1}:--with-%{2}}%%{!?with_%{1}:--without-%{2}}}
%define __without_if() %{expand:%%{?with_${1}:--without-%{2}}%%{!?with_%{1}:--with-%{2}}}

###################################################################
#
#  The Preamble
#  information that is displayed when users request info
#
###################################################################
Summary: Asterisk, The Open Source PBX
Name: asterisk
Version: %{actversion}
Release: %{actrelease}%{?_without_optimizations:_debug}%{dist}
License: GPL
Group: Utilities/System
Source: http://downloads.asterisk.org/pub/telephony/asterisk/old-releases/%{name}-%{version}.tar.gz
Source2: asterisk.logrotate
Source3: http://downloads.digium.com/pub/telephony/codec_opus/asterisk-%{astapi}.0/x86-64/codec_opus-%{astapi}.0_%{digium_opus_ver}-x86_64.tar.gz
Patch2: voicemail-splitopts.patch
Patch3: voicemail-splitopts-odbcstorage.patch
Patch4: voicemail-splitopts-imapstorage.patch
Patch5: ast-read-log-seqno.patch
Patch6: ari-subscriptions-early-free-fix.patch
Patch7: chan_sip-log-SIP_HEADER-errors.patch
Patch8: ari-bridge-list-prevent-fail-after-delete.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
URL: http://www.asterisk.org
Vendor: Digium, Inc.
Packager: Cloudonix <info@cloudonix.io>
Conflicts: asterisk10
Conflicts: asterisk11
Conflicts: asterisk12
Conflicts: asterisk13
Conflicts: asterisk14
Conflicts: asterisk15
Conflicts: asterisk16
Conflicts: asterisk17
Provides: asterisk%{astapi}
Obsoletes: asterisk%{astapi} < %{version}

#BuildRequires: asterisk-osp-devel

%{?_without_optimizations:Requires: %{name}-debuginfo = %{actversion}-%{release}}
Requires: %{name}-core = %{actversion}-%{release}
%if %{with dahdi}
Requires: %{name}-dahdi = %{actversion}-%{release}}
%endif
Requires: %{name}-doc = %{actversion}
Requires: %{name}-voicemail = %{actversion}-%{release}
Requires: asterisk-sounds-core-en-gsm
BuildRequires: gcc-c++ subversion
BuildRequires: sqlite-devel
BuildRequires: ncurses-devel
BuildRequires: libxml2-devel
%if 0%{?rhel} >= 8
BuildRequires: jansson-devel
%endif
BuildRequires: libuuid-devel
BuildRequires: libxslt-devel
BuildRequires: libsrtp-devel
%if %{with radius}
BuildRequires: radiusclient-ng-devel
%endif
BuildRequires: openldap-devel
BuildRequires: lua-devel
BuildRequires: spandsp-devel
%if %{with odbc}
BuildRequires: unixODBC-devel
%endif

# deps from Nir's script
%if 0%{?rhel} == 8
BuildRequires: python2-devel
%else
BuildRequires: python-devel
%endif
BuildRequires: libevent-devel
BuildRequires: unbound-devel
BuildRequires: zlib-devel
BuildRequires: openssl-devel
BuildRequires: gnutls-devel
BuildRequires: curl-devel
%if %{with snmp}
BuildRequires: net-snmp-devel
%endif
BuildRequires: neon-devel
BuildRequires: sqlite-devel
BuildRequires: gsm-devel
BuildRequires: gmime-devel
BuildRequires: mariadb-devel
BuildRequires: libedit-devel

%if %{with system_pjproject}
BuildRequires: pjproject-devel
%endif
%if %{with newt}
BuildRequires: newt-devel
%endif
BuildConflicts: rh-postgresql-devel

#
# Other tags not used
#
#Distribution:
#Icon:
#Conflicts:
#Serial:
#Provides:
#AutoReqProv:
#ExcludeArch:

%description
Asterisk is an open source PBX and telephony development platform.  Asterisk  
can both replace a conventional PBX and act as a platform for the
development of custom telephony applications for the delivery of dynamic
content over a telephone; similar to how one can deliver dynamic content 
through a web browser using CGI and a web server.

Asterisk supports a variety of telephony hardware including BRI, PRI, POTS,
and IP telephony clients using the Inter-Asterisk eXchange (IAX) protocol (e.g. 
gnophone or miniphone).  For more information and a current list of supported
hardware, see http://www.asterisk.org 

#
#  core subpackage
#
%package core
Summary: Asterisk core package without any "extras".
Group: Utilities/System
Provides: asterisk%{astapi}-core
Obsoletes: asterisk%{astapi}-core < %{version}
Requires: openssl
Requires: libxml2
Requires: sqlite
%if 0%{?rhel} >= 8
Requires: jansson
%endif
Requires: libuuid
Requires: libxslt
%if %{with system_pjproject}
Requires: pjproject
%endif

%description core
This package contains a base install of Asterisk without any "extras".

#
#  Alsa subpackage
#
%if %{with alsa}
%package alsa
Summary: Alsa channel driver for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-alsa
Obsoletes: asterisk%{astapi}-alsa < %{version}
%if "%{distname}" == "suse" || "%{distname}" == "sles"
BuildRequires: alsa-devel
Requires: alsa
%else
BuildRequires: alsa-lib-devel
Requires: alsa-lib
%endif
Requires: %{name}-core = %{actversion}-%{release}

%description alsa
Alsa channel driver for Asterisk 
%endif

#
#  snmp subpackage
#
%if %{with snmp}
%package snmp
Summary: snmp resource module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-snmp
Obsoletes: asterisk%{astapi}-snmp < %{version}
%if "%{distname}" == "suse" || "%{distname}" == "sles"
BuildRequires: sensors
BuildRequires: tcpd-devel
%else
BuildRequires: lm_sensors-devel
%endif
BuildRequires: net-snmp-devel
Requires: net-snmp
Requires: %{name}-core = %{actversion}-%{release}

%description snmp
snmp resource module for Asterisk 
%endif

#
#  pgsql subpackage
#
%if %{with pgsql}
%package pgsql
Summary: Postgresql modules for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-pgsql
Obsoletes: asterisk%{astapi}-pgsql < %{version}
BuildRequires: postgresql-devel
Requires: postgresql
Requires: %{name}-core = %{actversion}-%{release}

%description pgsql
Postgresql modules for Asterisk 
%endif

#
#  tds subpackage
#
%{?_without_tds:%if 0}
%{!?_without_tds:%if 1}
%package tds
Summary: tds modules for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-tds
Obsoletes: asterisk%{astapi}-tds < %{version}
BuildRequires: freetds-devel
Requires: freetds
Requires: %{name}-core = %{actversion}-%{release}

%description tds
tds modules for Asterisk 
%endif

#
#  dahdi subpackage
#
%if %{with dahdi}
%package dahdi
Summary: DAHDI channel driver for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-dahdi
Obsoletes: asterisk%{astapi}-dahdi < %{version}
Requires: %{name}-core = %{actversion}-%{release}
BuildRequires: libopenr2-devel
BuildRequires: libpri-devel
BuildRequires: libss7-devel
BuildRequires: libtonezone-devel
BuildRequires: dahdi-linux-devel
Requires: libopenr2
Requires: libpri
Requires: libss7
Requires: libtonezone
Requires: dahdi-linux
Requires: dahdi-linux-kmod
AutoReqProv: no

%description dahdi
DAHDI channel driver for Asterisk 
%endif

#
#  mISDN subpackage
#
%{?_without_misdn:%if 0}
%{!?_without_misdn:%if 1}
%package misdn
Summary: mISDN channel driver for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-misdn
Obsoletes: asterisk%{astapi}-misdn < %{version}
Requires: %{name}-core = %{actversion}-%{release}
BuildRequires: mISDNuser-devel
BuildRequires: mISDN-devel
Requires: mISDNuser
Requires: mISDN
Requires: mISDN-kmod
AutoReqProv: no

%description misdn
mISDN channel driver for Asterisk 
%endif

#
#  Configs subpackage
#
%package configs
Summary: Basic configuration files for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-configs
Obsoletes: asterisk%{astapi}-configs < %{version}
Requires: %{name}-core = %{actversion}-%{release}

%description configs
The sample configuration files for Asterisk

#
#  cURL subpackage
#
%{?_without_curl:%if 0}
%{!?_without_curl:%if 1}
%package curl
Summary: cURL application module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-curl
Obsoletes: asterisk%{astapi}-curl < %{version}
BuildRequires: curl-devel
Requires: curl
Requires: %{name}-core = %{actversion}-%{release}

%description curl
cURL application module for Asterisk 
%endif

#
#  Development subpackage
#
%package devel
Summary: Static libraries and header files for Asterisk development
Group: Development/Libraries
Provides: asterisk%{astapi}-devel
Obsoletes: asterisk%{astapi}-devel < %{version}
Requires: %{name}-core = %{actversion}-%{release}

%description devel
The static libraries and header files needed for building additional Asterisk
plugins/modules

#
#  Documentation subpackage
#
%package doc
Summary: Documentation files for Asterisk
Group: Development/Libraries
Provides: asterisk%{astapi}-doc
Obsoletes: asterisk%{astapi}-doc < %{version}
Requires: %{name}-core = %{actversion}-%{release}

%description doc
The Documentation files for Asterisk

#
#  Ogg-Vorbis subpackage
#
%if %{with ogg}
%package ogg
Summary: Ogg-Vorbis codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-ogg
Obsoletes: asterisk%{astapi}-ogg < %{version}
BuildRequires: libvorbis-devel libogg-devel
Requires: libvorbis libogg
Requires: %{name}-core = %{actversion}-%{release}

%description ogg
Asterisk format plugin for the Ogg-Vorbis codec
%endif

#
#  Speex subpackage
#
%if %{with speex}
%package speex
Summary: Speex codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-speex
Obsoletes: asterisk%{astapi}-speex < %{version}
BuildRequires: speex-devel
%if 0%{?rhel} >= 8
BuildRequires: speexdsp-devel
%endif
Requires: speex
Requires: %{name}-core = %{actversion}-%{release}

%description speex
Asterisk format plugin for the Speex codec
%endif

#
#  resample subpackage
#
%{?_without_resample:%if 0}
%{!?_without_resample:%if 1}
%package resample
Summary: resampling codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-resample
Obsoletes: asterisk%{astapi}-resample < %{version}
BuildRequires: libresample-devel
Requires: libresample
Requires: %{name}-core = %{actversion}-%{release}

%description resample
Asterisk plugin for the resample codec
%endif

#
#  unixODBC subpackage
#
%if %{with odbc}
%package odbc
Summary: Open Database Connectivity (ODBC) drivers for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-odbc
Obsoletes: asterisk%{astapi}-odbc < %{version}
BuildRequires: unixODBC-devel
Requires: unixODBC
Requires: %{name}-core = %{actversion}-%{release}

%description odbc
ODBC drivers for Asterisk 
%endif

#
#  sqlite subpackage
#
%if %{with sqlite}
%package sqlite
Summary: sqlite drivers for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-sqlite3 asterisk%{astapi}-sqlite
Obsoletes: asterisk%{astapi}-sqlite3  < %{version} asterisk%{astapi}-sqlite < %{version}
BuildRequires: sqlite-devel
Requires: sqlite
Requires: %{name}-core = %{actversion}-%{release}

%description sqlite
sqlite drivers for Asterisk 
%endif

#
#  voicemail file storage subpackage
#
%package voicemail
Summary: Voicemail with file storage module for Asterisk
Group: Utilities/System
Provides: %{name}-voicemail-filestorage = %{actversion}-%{release}
Provides: asterisk%{astapi}-voicemail
Obsoletes: asterisk%{astapi}-voicemail < %{version}
Provides: asterisk%{astapi}-voicemail-filestorage
Requires: %{name}-core = %{actversion}-%{release}

Conflicts: %{name}-voicemail-odbcstorage
Conflicts: %{name}-voicemail-imapstorage

%description voicemail
Voicemail with file storage module for Asterisk

#
#  voicemail ODBC storage subpackage
#
%{?_without_voicemail_odbcstorage:%if 0}
%{!?_without_voicemail_odbcstorage:%if 1}
%package voicemail-odbcstorage
Summary: Voicemail with ODBC storage module for Asterisk
Group: Utilities/System
BuildRequires: unixODBC-devel
Requires: unixODBC
Requires: %{name}-core = %{actversion}-%{release}
Provides: %{name}-voicemail = %{actversion}-%{release}
Provides: asterisk%{astapi}-voicemail
Provides: asterisk%{astapi}-voicemail-odbcstorage
Obsoletes: asterisk%{astapi}-voicemail-odbcstorage < %{version}

Conflicts: %{name}-voicemail-filestorage
Conflicts: %{name}-voicemail-imapstorage

%description voicemail-odbcstorage
Voicemail with ODBC storage module for Asterisk
%endif

#
#  voicemail IMAP storage subpackage
#
%{?_without_voicemail_imapstorage:%if 0}
%{!?_without_voicemail_imapstorage:%if 1}
%package voicemail-imapstorage
Summary: Voicemail with IMAP storage module for Asterisk
Group: Utilities/System
%if "%{distname}" == "suse" || "%{distname}" == "sles"
BuildRequires: imap-devel
Requires: imap-lib
%else
BuildRequires: libc-client-devel
Requires: libc-client
%endif
Requires: %{name}-core = %{actversion}-%{release}
Provides: %{name}-voicemail = %{actversion}-%{release}
Provides: asterisk%{astapi}-voicemail
Provides: asterisk%{astapi}-voicemail-imapstorage
Obsoletes: asterisk%{astapi}-voicemail-imapstorage < %{version}

Conflicts: %{name}-voicemail-filestorage
Conflicts: %{name}-voicemail-odbcstorage

%description voicemail-imapstorage
Voicemail with IMAP storage module for Asterisk
%endif

#
#  addons subpackage
#
%package addons
Summary: Asterisk-addons package.
Group: Utilities/System
Requires: asterisk%{astapi}-addons-core = %{actversion}-%{release}
Provides: asterisk-addons
Provides: asterisk%{astapi}-addons

Requires: %{name}-addons-core = %{actversion}-%{release}

%if %{with mysql}
Requires: %{name}-addons-mysql = %{actversion}-%{release}
Requires: mariadb
%endif

%if %{with bluetooth}
Requires: %{name}-addons-bluetooth = %{actversion}-%{release}
Requires: bluez-libs
%endif

%if %{with ooh323}
Requires: %{name}-addons-ooh323 = %{actversion}-%{release}
%endif

%description addons
This package contains a base install of Asterisk-addons without any "extras".

#
#  addons-core subpackage
#
%package addons-core
Summary: Asterisk-addons core package.
Group: Utilities/System
Provides: asterisk-gplonly
Requires: asterisk%{astapi}-core = %{actversion}-%{release}
Provides: asterisk-addons-core
Provides: asterisk%{astapi}-addons-core

%description addons-core
This package contains a base install of Asterisk-addons without any "extras".

#
#  addons-mysql subpackage
#
%if %{with mysql}
%package addons-mysql
Summary: mysql modules for Asterisk
Group: Utilities/System
BuildRequires: mariadb-devel
Requires: mysql
Requires: %{name}-addons-core = %{actversion}-%{release}
Provides: asterisk-addons-mysql
Provides: asterisk%{astapi}-addons-mysql

%description addons-mysql
mysql modules for Asterisk
%endif

#
#  bluetooth subpackage
#
%if %{with bluetooth}
%package addons-bluetooth
Summary: bluetooth modules for Asterisk
Group: Utilities/System
BuildRequires: bluez-libs-devel
Requires: bluez-libs
Requires: %{name}-core = %{actversion}-%{release}
Provides: asterisk-addons-bluetooth
Provides: asterisk%{astapi}-addons-bluetooth

%description addons-bluetooth
bluetooth modules for Asterisk 
%endif

#
#  ooh323 subpackage
#
%if %{with ooh323}
%package addons-ooh323
Summary: chan_ooh323 module for Asterisk
Group: Utilities/System
Requires: %{name}-core = %{actversion}-%{release}
Provides: asterisk-addons-ooh323
Provides: asterisk%{astapi}-addons-ooh323

%description addons-ooh323
chan_ooh323 module for Asterisk 
%endif

%if %{with opus}
%package opus-opensource
Summary: Opus codec module for Asterisk based on the open source libopus
Group: Utilities/System
Requires: %{name}-core = %{actversion}-%{release}
Provides: asterisk-opus
Provides: asterisk%{astapi}-opus
BuildRequires: opus-devel

%description opus-opensource
Opus module for Asterisk.
%endif

%if %{with digium_opus}
%package opus
Summary: Opus codec module for Asterisk from Digium
Group: Utilities/System
Requires: %{name}-core = %{actversion}-%{release}
Provides: asterisk-opus
Provides: asterisk%{astapi}-opus

%description opus
Opus module for Asterisk, from Digium. See codec_opus/LICENSE for details.
%endif

###################################################################
#
#  The Prep Section
#  If stuff needs to be done before building, this is the section
#  Use shell scripts to do stuff like uncompress and cd into source dir 
#  %setup macro - cleans old build trees, uncompress and extracts original source 
#
###################################################################
%prep
%define _default_patch_fuzz 3
%setup -n %{name}-%{aversion}
%if %{with digium_opus}
%setup -b 3
mv ../codec_opus-%{astapi}.0_%{digium_opus_ver}-x86_64/ codec_opus
%endif

# Create copies for odbcstorage and imapstorage voicemail modules.
%{?_without_voicemail_odbcstorage:%if 0}
%{!?_without_voicemail_odbcstorage:%if 1}
cp apps/app_voicemail.c apps/app_voicemail_odbcstorage.c
cp apps/app_voicemail.exports.in apps/app_voicemail_odbcstorage.exports.in
%patch3 -p0
%endif

%{?_without_voicemail_imapstorage:%if 0}
%{!?_without_voicemail_imapstorage:%if 1}
cp apps/app_voicemail.c apps/app_voicemail_imapstorage.c
cp apps/app_voicemail.exports.in apps/app_voicemail_imapstorage.exports.in
%patch4 -p0
%endif

%if %{with opus}
curl -L https://github.com/cloudonix/asterisk-opus/archive/asterisk-16.0.tar.gz | tar -zx --xform=s,-asterisk-16.0,,
cp -R asterisk-opus/codecs/* codecs/
cp -R asterisk-opus/include/* include/
cp -R asterisk-opus/res/* res/
%endif

%{?_without_voicemail_splitopts:%if 0}
%{!?_without_voicemail_splitopts:%if 1}
%patch2 -p0
%endif

%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1

./bootstrap.sh

###################################################################
#
#  The Build Section
#  Use shell scripts and do stuff that makes the build happen, i.e. make
#
###################################################################
%build
%ifarch x86_64
%define makeflags OPT=-fPIC
%else
%define makeflags OPT=
%endif
echo %{aversion}%{?_without_optimizations:-debug} > .version

%configure \
%if %{with system_pjproject}
	--with-pjproject-bundled \
%endif
%if 0%{?rhel} >= 8
	--with-jansson \
%else
	--with-jansson-bundled \
%endif
	--with-unbound \
	--with-srtp \
	%{__without oss SDL_image} \
	%{__without bluetooth bluetooth} \
	--without-gtk2 \
	--with-gnu-ld \
	%{__without ilbc ilbc} \
	%{__without jack jack} \
	%{__without ldap ldap} \
	%{__without mysql mysqlclient} \
	%{__without oss oss} \
	%{__without pjsip pjproject} \
	%{__without portaudio portaudio} \
	%{__without pgsql postgres} \
	%{__without radius radius} \
	%{__without oss sdl} \
	%{__without tds tds} \
	%{__without odbc unixodbc}

%{__make} menuselect.makeopts

menuselect/menuselect \
	--disable-category MENUSELECT_CORE_SOUNDS \
	--disable-category MENUSELECT_EXTRA_SOUNDS \
	--disable-category MENUSELECT_MOH \
	--disable-category MENUSELECT_ADDONS \
	--enable app_meetme \
	--enable app_page \
	--disable chan_mgcp \
	--disable chan_iax2 \
	--disable chan_motif \
	--disable chan_nbs \
	--disable chan_phone \
	--disable chan_skinny \
	--disable chan_unistim \
	--disable codec_g726 \
	--disable codec_lpc10 \
	--disable res_adsi \
	--disable res_xmpp \
	--enable format_mp3 \
	menuselect.makeopts

%if %{without sqlite}
menuselect/menuselect --disable res_config_sqlite3 cdr_sqlite3_custom cel_sqlite3_custom menuselect.makeopts
%endif
%if %{without oss}
menuselect/menuselect --disable chan_oss menuselect.makeopts
%endif
%if %{without tds}
menuselect/menuselect --disable cdr_tds --disable cel_tds menuselect.makeopts
%endif
%if %{without ilbc}
menuselect/menuselect --disable codec_ilbc --disable format_ilbc menuselect.makeopts
%endif
%if %{without ldap}
menuselect/menuselect --disable res_config_ldap menuselect.makeopts
%endif
%if %{without bluetooth}
menuselect/menuselect --disable chan_mobile menuselect.makeopts
%endif
%if %{without jack}
menuselect/menuselect --disable app_jack menuselect.makeopts
%endif
%if %{with mysql}
menuselect/menuselect --enable res_config_mysql --enable app_mysql --enable cdr_mysql menuselect.makeopts
%endif
%if %{without pgsql}
menuselect/menuselect --disable res_config_pgsql --disable cdr_pgsql --disable cel_pgsql menuselect.makeopts
%endif
%if %{without odbc}
menuselect/menuselect --disable res_odbc --disable res_config_odbc --disable cdr_odbc --disable cdr_adaptive_odbc --disable cel_odbc menuselect.makeopts
%endif
%if %{without radius}
menuselect/menuselect --disable cdr_radius --disable cel_radius menuselect.makeopts
%endif
%if %{without pjsip}
menuselect/menuselect --disable res_pjsip --disable chan_pjsip menuselect.makeopts
%endif
%if %{with opus}
menuselect/menuselect --enable codec_opus_open_source menuselect.makeopts
%endif
%if %{with ooh323}
menuselect/menuselect --enable chan_ooh323 menuselect.makeopts
%endif


%if %{with malloc_debug}
menuselect/menuselect --enable MALLOC_DEBUG menuselect.makeopts
%else
menuselect/menuselect --disable MALLOC_DEBUG menuselect.makeopts
%endif

menuselect/menuselect --check-deps menuselect.makeopts

contrib/scripts/get_mp3_source.sh

make %{makeflags}

###################################################################
#
#  The Install Section
#  Use shell scripts and perform the install, like 'make install', 
#  but can also be shell commands, i.e. cp, mv, install, etc.. 
#
###################################################################
%install
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/rc.d/init.d/
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/
echo "AST_USER=asterisk" > $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/asterisk
echo "AST_GROUP=asterisk" >> $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/asterisk

make DESTDIR=$RPM_BUILD_ROOT install WGET_EXTRA_ARGS="--no-verbose"
make DESTDIR=$RPM_BUILD_ROOT install-headers
make DESTDIR=$RPM_BUILD_ROOT samples
make DESTDIR=$RPM_BUILD_ROOT config

mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/logrotate.d/
cp %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/asterisk

mkdir -p $RPM_BUILD_ROOT/var/lib/asterisk/licenses/
mkdir -p $RPM_BUILD_ROOT/var/lib/digium/licenses/

%if %{with digium_opus}
install -D -m644 codec_opus/format_ogg_opus.so $RPM_BUILD_ROOT%{_libdir}/asterisk/modules/format_ogg_opus.so
install -D -m644 codec_opus/codec_opus.so $RPM_BUILD_ROOT%{_libdir}/asterisk/modules/codec_opus.so
install -D -m644 codec_opus/codec_opus_config-en_US.xml $RPM_BUILD_ROOT/var/lib/asterisk/documentation/thirdparty/codec_opus_config-en_US.xml
install -D -m644 codec_opus/README $RPM_BUILD_ROOT/var/lib/asterisk/documentation/thirdparty/codec_opus/README
install -D -m644 codec_opus/LICENSE $RPM_BUILD_ROOT/var/lib/asterisk/documentation/thirdparty/codec_opus/LICENSE
%endif

%if 0%{?rhel} >= 8
# EL8 builds complain about ambigous python because they no longer support just running 'python'
LC_ALL=C perl -pi -e 's/env python/env python2/' $RPM_BUILD_ROOT/var/lib/asterisk/scripts/*.py
%endif

###################################################################
#
#  Install and Uninstall 
#  This section can have scripts that are run either before/after
#  an install process, or before/after an uninstall process
#  %pre - executes prior to the installation of a package
#  %post - executes after the package is installed
#  %preun - executes prior to the uninstallation of a package
#  %postun - executes after the uninstallation of a package
#
###################################################################
%pre core
useradd -r -M -s /sbin/nologin -d /var/lib/asterisk asterisk 2> /dev/null || :

%post core
chkconfig --add asterisk

###################################################################
#
#  Verify
#
###################################################################
%verifyscript

###################################################################
#
#  Clean
#
###################################################################
%clean
cd $RPM_BUILD_DIR
%{__rm} -rf %{name}-%{version} 
%{__rm} -rf /var/log/%{name}-sources-%{version}-%{release}.make.err
%{__rm} -rf $RPM_BUILD_ROOT

###################################################################
#
#  File List
#
###################################################################
%files
%defattr(-, root, root)
%exclude /var/lib/asterisk/scripts/

%files core
%defattr(-, root, root)
%config %{_sysconfdir}/rc.d/init.d/asterisk
%config %{_sysconfdir}/sysconfig/asterisk
%attr(0775,asterisk,asterisk) %dir %{_sysconfdir}/asterisk
#
# DAHDI span config script
#
#/usr/share/dahdi/span_config.d/40-asterisk
#
#  Module List
#
%{_libdir}/asterisk/modules/app_agent_pool.so
%{_libdir}/asterisk/modules/app_alarmreceiver.so
%{_libdir}/asterisk/modules/app_amd.so
%{_libdir}/asterisk/modules/app_audiosocket.so
%{_libdir}/asterisk/modules/app_authenticate.so
%{_libdir}/asterisk/modules/app_bridgewait.so
%{_libdir}/asterisk/modules/app_broadcast.so
%{_libdir}/asterisk/modules/app_cdr.so
%{_libdir}/asterisk/modules/app_celgenuserevent.so
%{_libdir}/asterisk/modules/app_chanisavail.so
%{_libdir}/asterisk/modules/app_channelredirect.so
%{_libdir}/asterisk/modules/app_chanspy.so
%{_libdir}/asterisk/modules/app_confbridge.so
%{_libdir}/asterisk/modules/app_controlplayback.so
%{_libdir}/asterisk/modules/app_db.so
%{_libdir}/asterisk/modules/app_dial.so
%{_libdir}/asterisk/modules/app_dictate.so
%{_libdir}/asterisk/modules/app_directed_pickup.so
%{_libdir}/asterisk/modules/app_directory.so
%{_libdir}/asterisk/modules/app_disa.so
%{_libdir}/asterisk/modules/app_dtmfstore.so
%{_libdir}/asterisk/modules/app_dumpchan.so
%{_libdir}/asterisk/modules/app_echo.so
%{_libdir}/asterisk/modules/app_exec.so
%{_libdir}/asterisk/modules/app_externalivr.so
%{_libdir}/asterisk/modules/app_festival.so
%{_libdir}/asterisk/modules/app_forkcdr.so
%{_libdir}/asterisk/modules/app_followme.so
%{_libdir}/asterisk/modules/app_ices.so
%{_libdir}/asterisk/modules/app_if.so
%{_libdir}/asterisk/modules/app_image.so
%{_libdir}/asterisk/modules/app_milliwatt.so
%{_libdir}/asterisk/modules/app_minivm.so
%{_libdir}/asterisk/modules/app_mixmonitor.so
%{_libdir}/asterisk/modules/app_morsecode.so
%{_libdir}/asterisk/modules/app_mf.so
%{_libdir}/asterisk/modules/app_mp3.so
%{_libdir}/asterisk/modules/app_nbscat.so
%{_libdir}/asterisk/modules/app_originate.so
%{_libdir}/asterisk/modules/app_page.so
%{_libdir}/asterisk/modules/app_playback.so
%{_libdir}/asterisk/modules/app_playtones.so
%{_libdir}/asterisk/modules/app_privacy.so
%{_libdir}/asterisk/modules/app_queue.so
%{_libdir}/asterisk/modules/app_readexten.so
%{_libdir}/asterisk/modules/app_read.so
%{_libdir}/asterisk/modules/app_record.so
%{_libdir}/asterisk/modules/app_reload.so
%{_libdir}/asterisk/modules/app_sayunixtime.so
%{_libdir}/asterisk/modules/app_senddtmf.so
%{_libdir}/asterisk/modules/app_sendtext.so
%{_libdir}/asterisk/modules/app_sf.so
%{_libdir}/asterisk/modules/app_signal.so
%{_libdir}/asterisk/modules/app_sms.so
%{_libdir}/asterisk/modules/app_softhangup.so
%{_libdir}/asterisk/modules/app_speech_utils.so
%{_libdir}/asterisk/modules/app_stack.so
%{_libdir}/asterisk/modules/app_stasis.so
%{_libdir}/asterisk/modules/app_system.so
%{_libdir}/asterisk/modules/app_talkdetect.so
%{_libdir}/asterisk/modules/app_test.so
%{_libdir}/asterisk/modules/app_transfer.so
%{_libdir}/asterisk/modules/app_url.so
%{_libdir}/asterisk/modules/app_userevent.so
%{_libdir}/asterisk/modules/app_verbose.so
%{_libdir}/asterisk/modules/app_waitforcond.so
%{_libdir}/asterisk/modules/app_waitforring.so
%{_libdir}/asterisk/modules/app_waitforsilence.so
%{_libdir}/asterisk/modules/app_waituntil.so
%{_libdir}/asterisk/modules/app_while.so
%{_libdir}/asterisk/modules/app_zapateller.so
%{_libdir}/asterisk/modules/app_attended_transfer.so
%{_libdir}/asterisk/modules/app_blind_transfer.so
%{_libdir}/asterisk/modules/app_stream_echo.so
%{_libdir}/asterisk/modules/bridge_builtin_features.so
%{_libdir}/asterisk/modules/bridge_builtin_interval_features.so
%{_libdir}/asterisk/modules/bridge_holding.so
%{_libdir}/asterisk/modules/bridge_native_rtp.so
%{_libdir}/asterisk/modules/bridge_simple.so
%{_libdir}/asterisk/modules/bridge_softmix.so
%{_libdir}/asterisk/modules/cdr_csv.so
%{_libdir}/asterisk/modules/cdr_custom.so
%{_libdir}/asterisk/modules/cdr_manager.so
%{_libdir}/asterisk/modules/cel_custom.so
%{_libdir}/asterisk/modules/cel_manager.so
%{_libdir}/asterisk/modules/chan_audiosocket.so
%{_libdir}/asterisk/modules/chan_bridge_media.so
%{_libdir}/asterisk/modules/chan_sip.so
%{_libdir}/asterisk/modules/codec_adpcm.so
%{_libdir}/asterisk/modules/codec_alaw.so
%{_libdir}/asterisk/modules/codec_a_mu.so
%{_libdir}/asterisk/modules/codec_g722.so
%{_libdir}/asterisk/modules/codec_gsm.so
%{_libdir}/asterisk/modules/codec_ilbc.so
%{_libdir}/asterisk/modules/codec_ulaw.so
%{_libdir}/asterisk/modules/format_g719.so
%{_libdir}/asterisk/modules/format_g723.so
%{_libdir}/asterisk/modules/format_g726.so
%{_libdir}/asterisk/modules/format_g729.so
%{_libdir}/asterisk/modules/format_gsm.so
%{_libdir}/asterisk/modules/format_h263.so
%{_libdir}/asterisk/modules/format_h264.so
%{_libdir}/asterisk/modules/format_ilbc.so
%{_libdir}/asterisk/modules/format_pcm.so
%{_libdir}/asterisk/modules/format_siren7.so
%{_libdir}/asterisk/modules/format_siren14.so
%{_libdir}/asterisk/modules/format_sln.so
%{_libdir}/asterisk/modules/format_vox.so
%{_libdir}/asterisk/modules/format_wav_gsm.so
%{_libdir}/asterisk/modules/format_wav.so
%{_libdir}/asterisk/modules/func_aes.so
%{_libdir}/asterisk/modules/func_base64.so
%{_libdir}/asterisk/modules/func_blacklist.so
%{_libdir}/asterisk/modules/func_callcompletion.so
%{_libdir}/asterisk/modules/func_callerid.so
%{_libdir}/asterisk/modules/func_cdr.so
%{_libdir}/asterisk/modules/func_channel.so
%{_libdir}/asterisk/modules/func_config.so
%{_libdir}/asterisk/modules/func_cut.so
%{_libdir}/asterisk/modules/func_db.so
%{_libdir}/asterisk/modules/func_devstate.so
%{_libdir}/asterisk/modules/func_dialgroup.so
%{_libdir}/asterisk/modules/func_dialplan.so
%{_libdir}/asterisk/modules/func_enum.so
%{_libdir}/asterisk/modules/func_env.so
%{_libdir}/asterisk/modules/func_evalexten.so
%{_libdir}/asterisk/modules/func_export.so
%{_libdir}/asterisk/modules/func_extstate.so
%{_libdir}/asterisk/modules/func_frame_drop.so
%{_libdir}/asterisk/modules/func_frame_trace.so
%{_libdir}/asterisk/modules/func_global.so
%{_libdir}/asterisk/modules/func_groupcount.so
%{_libdir}/asterisk/modules/func_hangupcause.so
%{_libdir}/asterisk/modules/func_iconv.so
%{_libdir}/asterisk/modules/func_jitterbuffer.so
%{_libdir}/asterisk/modules/func_json.so
%{_libdir}/asterisk/modules/func_lock.so
%{_libdir}/asterisk/modules/func_logic.so
%{_libdir}/asterisk/modules/func_math.so
%{_libdir}/asterisk/modules/func_md5.so
%{_libdir}/asterisk/modules/func_module.so
%{_libdir}/asterisk/modules/func_periodic_hook.so
%{_libdir}/asterisk/modules/func_pitchshift.so
%{_libdir}/asterisk/modules/func_presencestate.so
%{_libdir}/asterisk/modules/func_rand.so
%{_libdir}/asterisk/modules/func_realtime.so
%{_libdir}/asterisk/modules/func_sayfiles.so
%{_libdir}/asterisk/modules/func_scramble.so
%{_libdir}/asterisk/modules/func_sha1.so
%{_libdir}/asterisk/modules/func_shell.so
%{_libdir}/asterisk/modules/func_sorcery.so
%{_libdir}/asterisk/modules/func_sprintf.so
%{_libdir}/asterisk/modules/func_srv.so
%{_libdir}/asterisk/modules/func_strings.so
%{_libdir}/asterisk/modules/func_sysinfo.so
%{_libdir}/asterisk/modules/func_talkdetect.so
%{_libdir}/asterisk/modules/func_timeout.so
%{_libdir}/asterisk/modules/func_uri.so
%{_libdir}/asterisk/modules/func_version.so
%{_libdir}/asterisk/modules/func_vmcount.so
%{_libdir}/asterisk/modules/func_volume.so
%{_libdir}/asterisk/modules/pbx_ael.so
%{_libdir}/asterisk/modules/pbx_config.so
%{_libdir}/asterisk/modules/pbx_dundi.so
%{_libdir}/asterisk/modules/pbx_loopback.so
%{_libdir}/asterisk/modules/pbx_realtime.so
%{_libdir}/asterisk/modules/pbx_spool.so
%{_libdir}/asterisk/modules/res_aeap.so
%{_libdir}/asterisk/modules/res_ael_share.so
%{_libdir}/asterisk/modules/res_agi.so
%{_libdir}/asterisk/modules/res_ari_applications.so
%{_libdir}/asterisk/modules/res_ari_asterisk.so
%{_libdir}/asterisk/modules/res_ari_bridges.so
%{_libdir}/asterisk/modules/res_ari_channels.so
%{_libdir}/asterisk/modules/res_ari_device_states.so
%{_libdir}/asterisk/modules/res_ari_endpoints.so
%{_libdir}/asterisk/modules/res_ari_events.so
%{_libdir}/asterisk/modules/res_ari_model.so
%{_libdir}/asterisk/modules/res_ari_playbacks.so
%{_libdir}/asterisk/modules/res_ari_recordings.so
%{_libdir}/asterisk/modules/res_ari.so
%{_libdir}/asterisk/modules/res_ari_sounds.so
%{_libdir}/asterisk/modules/res_audiosocket.so
%{_libdir}/asterisk/modules/res_calendar.so
%{_libdir}/asterisk/modules/res_clialiases.so
%{_libdir}/asterisk/modules/res_clioriginate.so
%{_libdir}/asterisk/modules/res_convert.so
%{_libdir}/asterisk/modules/res_crypto.so
%{_libdir}/asterisk/modules/res_fax.so
%{_libdir}/asterisk/modules/res_format_attr_celt.so
%{_libdir}/asterisk/modules/res_format_attr_h263.so
%{_libdir}/asterisk/modules/res_format_attr_h264.so
%{_libdir}/asterisk/modules/res_format_attr_silk.so
%{_libdir}/asterisk/modules/res_geolocation.so
%{_libdir}/asterisk/modules/res_hep.so
%{_libdir}/asterisk/modules/res_hep_rtcp.so
%{_libdir}/asterisk/modules/res_http_websocket.so
%{_libdir}/asterisk/modules/res_limit.so
%{_libdir}/asterisk/modules/res_manager_devicestate.so
%{_libdir}/asterisk/modules/res_manager_presencestate.so
%{_libdir}/asterisk/modules/res_monitor.so
%{_libdir}/asterisk/modules/res_musiconhold.so
%{_libdir}/asterisk/modules/res_mwi_devstate.so
%{_libdir}/asterisk/modules/res_mutestream.so
%{_libdir}/asterisk/modules/res_parking.so
%{_libdir}/asterisk/modules/res_phoneprov.so
%{_libdir}/asterisk/modules/res_prometheus.so
%{_libdir}/asterisk/modules/res_realtime.so
%{_libdir}/asterisk/modules/res_rtp_asterisk.so
%{_libdir}/asterisk/modules/res_rtp_multicast.so
%{_libdir}/asterisk/modules/res_security_log.so
%{_libdir}/asterisk/modules/res_smdi.so
%{_libdir}/asterisk/modules/res_sorcery_astdb.so
%{_libdir}/asterisk/modules/res_sorcery_config.so
%{_libdir}/asterisk/modules/res_sorcery_memory.so
%{_libdir}/asterisk/modules/res_sorcery_realtime.so
%{_libdir}/asterisk/modules/res_speech.so
%{_libdir}/asterisk/modules/res_speech_aeap.so
%{_libdir}/asterisk/modules/res_srtp.so
%{_libdir}/asterisk/modules/res_stasis_answer.so
%{_libdir}/asterisk/modules/res_stasis_device_state.so
%{_libdir}/asterisk/modules/res_stasis_playback.so
%{_libdir}/asterisk/modules/res_stasis_recording.so
%{_libdir}/asterisk/modules/res_stasis_snoop.so
%{_libdir}/asterisk/modules/res_stasis.so
%{_libdir}/asterisk/modules/res_statsd.so
%{_libdir}/asterisk/modules/res_stir_shaken.so
%{_libdir}/asterisk/modules/res_stun_monitor.so
%{_libdir}/asterisk/modules/res_tonedetect.so
%{_libdir}/asterisk/modules/res_timing_pthread.so
%{_libdir}/asterisk/modules/app_bridgeaddchan.so
%{_libdir}/asterisk/modules/chan_rtp.so
%{_libdir}/asterisk/modules/codec_resample.so
%{_libdir}/asterisk/modules/func_holdintercept.so
%{_libdir}/asterisk/modules/pbx_lua.so
%{_libdir}/asterisk/modules/res_calendar_ews.so
%{_libdir}/asterisk/modules/res_fax_spandsp.so
%{_libdir}/asterisk/modules/res_format_attr_g729.so
%{_libdir}/asterisk/modules/res_format_attr_ilbc.so
%{_libdir}/asterisk/modules/res_format_attr_siren14.so
%{_libdir}/asterisk/modules/res_format_attr_siren7.so
%{_libdir}/asterisk/modules/res_format_attr_vp8.so
%{_libdir}/asterisk/modules/res_http_media_cache.so
%{_libdir}/asterisk/modules/res_http_post.so
%if %{with pjsip}
%{_libdir}/asterisk/modules/res_pjproject.so
%endif
%{_libdir}/asterisk/modules/res_resolver_unbound.so
%{_libdir}/asterisk/modules/res_sorcery_memory_cache.so

%if %{with oss}
%{_libdir}/asterisk/modules/chan_oss.so
%endif
%if %{with pjsip}
%{_libdir}/asterisk/modules/chan_pjsip.so
%{_libdir}/asterisk/modules/func_pjsip_aor.so
%{_libdir}/asterisk/modules/func_pjsip_contact.so
%{_libdir}/asterisk/modules/func_pjsip_endpoint.so
%{_libdir}/asterisk/modules/res_hep_pjsip.so
%{_libdir}/asterisk/modules/res_pjsip_acl.so
%{_libdir}/asterisk/modules/res_pjsip_authenticator_digest.so
%{_libdir}/asterisk/modules/res_pjsip_caller_id.so
%{_libdir}/asterisk/modules/res_pjsip_config_wizard.so
%{_libdir}/asterisk/modules/res_pjsip_dialog_info_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_diversion.so
%{_libdir}/asterisk/modules/res_pjsip_dtmf_info.so
%{_libdir}/asterisk/modules/res_pjsip_endpoint_identifier_anonymous.so
%{_libdir}/asterisk/modules/res_pjsip_endpoint_identifier_ip.so
%{_libdir}/asterisk/modules/res_pjsip_endpoint_identifier_user.so
%{_libdir}/asterisk/modules/res_pjsip_exten_state.so
%{_libdir}/asterisk/modules/res_pjsip_header_funcs.so
%{_libdir}/asterisk/modules/res_pjsip_logger.so
%{_libdir}/asterisk/modules/res_pjsip_messaging.so
%{_libdir}/asterisk/modules/res_pjsip_mwi.so
%{_libdir}/asterisk/modules/res_pjsip_mwi_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_nat.so
%{_libdir}/asterisk/modules/res_pjsip_notify.so
%{_libdir}/asterisk/modules/res_pjsip_one_touch_record_info.so
%{_libdir}/asterisk/modules/res_pjsip_outbound_authenticator_digest.so
%{_libdir}/asterisk/modules/res_pjsip_outbound_publish.so
%{_libdir}/asterisk/modules/res_pjsip_outbound_registration.so
%{_libdir}/asterisk/modules/res_pjsip_path.so
%{_libdir}/asterisk/modules/res_pjsip_phoneprov_provider.so
%{_libdir}/asterisk/modules/res_pjsip_pidf_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_pidf_digium_body_supplement.so
%{_libdir}/asterisk/modules/res_pjsip_pidf_eyebeam_body_supplement.so
%{_libdir}/asterisk/modules/res_pjsip_pubsub.so
%{_libdir}/asterisk/modules/res_pjsip_publish_asterisk.so
%{_libdir}/asterisk/modules/res_pjsip_refer.so
%{_libdir}/asterisk/modules/res_pjsip_registrar.so
%{_libdir}/asterisk/modules/res_pjsip_rfc3326.so
%{_libdir}/asterisk/modules/res_pjsip_sdp_rtp.so
%{_libdir}/asterisk/modules/res_pjsip_send_to_voicemail.so
%{_libdir}/asterisk/modules/res_pjsip_session.so
%{_libdir}/asterisk/modules/res_pjsip_sips_contact.so
%{_libdir}/asterisk/modules/res_pjsip.so
%{_libdir}/asterisk/modules/res_pjsip_t38.so
%{_libdir}/asterisk/modules/res_pjsip_transport_websocket.so
%{_libdir}/asterisk/modules/res_pjsip_xpidf_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_dlg_options.so
%{_libdir}/asterisk/modules/res_pjsip_empty_info.so
%{_libdir}/asterisk/modules/res_pjsip_history.so
%endif
%if %{with radius}
%{_libdir}/asterisk/modules/cdr_radius.so
%{_libdir}/asterisk/modules/cel_radius.so
%endif
%if %{with ldap}
%{_libdir}/asterisk/modules/res_config_ldap.so
%endif

%{!?el5:%{_libdir}/asterisk/modules/res_timing_timerfd.so}
%{_sbindir}/asterisk
%{_sbindir}/rasterisk
%{_sbindir}/safe_asterisk
%{_sbindir}/astdb2bdb
%{_sbindir}/astdb2sqlite3
%{_sbindir}/astcanary
%{_sbindir}/astgenkey
%{_sbindir}/astversion
%{_sbindir}/autosupport

%{_libdir}/libasteriskssl.so
%{_libdir}/libasteriskssl.so.1
%if %{with pjsip}
%{_libdir}/libasteriskpj.so
%{_libdir}/libasteriskpj.so.2
%endif

#
#  CDR Log Directories
#
%attr(0775,asterisk,asterisk) %dir %{logdir}/asterisk
%attr(0775,asterisk,asterisk) %dir %{logdir}/asterisk/cdr-csv
%attr(0775,asterisk,asterisk) %dir %{logdir}/asterisk/cdr-custom

#
#  logrotate config
#
%config(noreplace) %{_sysconfdir}/logrotate.d/asterisk

#
#  run Directory
#
%attr(0775,asterisk,asterisk) %dir /var/run/asterisk

%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/agi-bin
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/documentation
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/documentation/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/images
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/images/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/keys
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/licenses
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/phoneprov
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/phoneprov/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/rest-api
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/rest-api/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/static-http
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/static-http/*

%attr(0755,asterisk,asterisk) %dir /var/lib/digium
%attr(0755,asterisk,asterisk) %dir /var/lib/digium/licenses

#
#  Spool Directories
#
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/meetme
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/system
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/tmp
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/voicemail


#
#  Alsa Subpackage
#
%if %{with alsa}
%files alsa
%defattr(-, root, root)
%{_libdir}/asterisk/modules/chan_alsa.so
%endif

#
#  snmp Subpackage
#
%if %{with snmp}
%files snmp
%defattr(-, root, root)
%{_libdir}/asterisk/modules/res_snmp.so
%endif

#
#  pgsql Subpackage
#
%if %{with pgsql}
%files pgsql
%defattr(-, root, root)
%{_libdir}/asterisk/modules/res_config_pgsql.so
%{_libdir}/asterisk/modules/cdr_pgsql.so
%{_libdir}/asterisk/modules/cel_pgsql.so
%endif

#
#  tds Subpackage
#
%if %{with tds}
%files tds
%defattr(-, root, root)
%{_libdir}/asterisk/modules/cdr_tds.so
%{_libdir}/asterisk/modules/cel_tds.so
%endif

#
#  sqlite package
#
%if %{with sqlite}
%files sqlite
%defattr(-, root, root)
%{_libdir}/asterisk/modules/cdr_sqlite3_custom.so
%{_libdir}/asterisk/modules/cel_sqlite3_custom.so
%{_libdir}/asterisk/modules/res_config_sqlite3.so
%endif

#
#  mISDN Subpackage
#
%{?_without_misdn:%if 0}
%{!?_without_misdn:%if 1}
%files misdn
%defattr(-, root, root)
%{_libdir}/asterisk/modules/chan_misdn.so
%endif

#
#  dahdi Subpackage
#
%if %{with dahdi}
%files dahdi
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_dahdiras.so
%{_libdir}/asterisk/modules/app_flash.so
%{_libdir}/asterisk/modules/app_meetme.so
%{_libdir}/asterisk/modules/chan_dahdi.so
%{_libdir}/asterisk/modules/codec_dahdi.so
%{_libdir}/asterisk/modules/res_timing_dahdi.so
%endif

#
#  Configs Subpackage
#
%files configs
%defattr(-, asterisk, asterisk)
%attr(0664,asterisk,asterisk) %config(noreplace) %{_sysconfdir}/asterisk/*

#
#  cURL Subpackage
#
%{?_without_curl:%if 0}
%{!?_without_curl:%if 1}
%files curl
%defattr(-, root, root)
%{_libdir}/asterisk/modules/func_curl.so
%{_libdir}/asterisk/modules/res_config_curl.so
%{_libdir}/asterisk/modules/res_curl.so
%endif

#
#  Development Subpackage
#
%files devel
%defattr(-, root, root)
%{_includedir}/asterisk.h
%{_includedir}/asterisk/_private.h
%{_includedir}/asterisk/abstract_jb.h
%{_includedir}/asterisk/acl.h
%{_includedir}/asterisk/adsi.h
%{_includedir}/asterisk/ael_structs.h
%{_includedir}/asterisk/agi.h
%{_includedir}/asterisk/alaw.h
%{_includedir}/asterisk/aoc.h
%{_includedir}/asterisk/autochan.h
%{_includedir}/asterisk/autoconfig.h
%{_includedir}/asterisk/app.h
%{_includedir}/asterisk/ari.h
%{_includedir}/asterisk/astdb.h
%{_includedir}/asterisk/ast_expr.h
%{_includedir}/asterisk/ast_version.h
%{_includedir}/asterisk/astmm.h
%{_includedir}/asterisk/astobj2.h
%{_includedir}/asterisk/audiohook.h
%{_includedir}/asterisk/backtrace.h
%{_includedir}/asterisk/beep.h
%{_includedir}/asterisk/bridge_after.h
%{_includedir}/asterisk/bridge_basic.h
%{_includedir}/asterisk/bridge_channel.h
%{_includedir}/asterisk/bridge_channel_internal.h
%{_includedir}/asterisk/bridge_features.h
%{_includedir}/asterisk/bridge.h
%{_includedir}/asterisk/bridge_internal.h
%{_includedir}/asterisk/bridge_roles.h
%{_includedir}/asterisk/bridge_technology.h
%{_includedir}/asterisk/bucket.h
%{_includedir}/asterisk/build.h
%{_includedir}/asterisk/buildinfo.h
%{_includedir}/asterisk/buildopts.h
%{_includedir}/asterisk/calendar.h
%{_includedir}/asterisk/callerid.h
%{_includedir}/asterisk/causes.h
%{_includedir}/asterisk/ccss.h
%{_includedir}/asterisk/cdr.h
%{_includedir}/asterisk/cel.h
%{_includedir}/asterisk/celt.h
%{_includedir}/asterisk/channel.h
%{_includedir}/asterisk/channel_internal.h
%{_includedir}/asterisk/channelstate.h
%{_includedir}/asterisk/chanvars.h
%{_includedir}/asterisk/cli.h
%{_includedir}/asterisk/codec.h
%{_includedir}/asterisk/compat.h
%{_includedir}/asterisk/compiler.h
%{_includedir}/asterisk/config.h
%{_includedir}/asterisk/config_options.h
%{_includedir}/asterisk/core_local.h
%{_includedir}/asterisk/core_unreal.h
%{_includedir}/asterisk/crypto.h
%{_includedir}/asterisk/datastore.h
%{_includedir}/asterisk/devicestate.h
%{_includedir}/asterisk/dial.h
%{_includedir}/asterisk/dlinkedlists.h
%{_includedir}/asterisk/dns.h
%{_includedir}/asterisk/dnsmgr.h
%{_includedir}/asterisk/doxyref.h
%{_includedir}/asterisk/dsp.h
%{_includedir}/asterisk/dundi.h
%{_includedir}/asterisk/endian.h
%{_includedir}/asterisk/endpoints.h
%{_includedir}/asterisk/enum.h
%{_includedir}/asterisk/event.h
%{_includedir}/asterisk/event_defs.h
%{_includedir}/asterisk/extconf.h
%{_includedir}/asterisk/features_config.h
%{_includedir}/asterisk/features.h
%{_includedir}/asterisk/file.h
%{_includedir}/asterisk/format.h
%{_includedir}/asterisk/format_cache.h
%{_includedir}/asterisk/format_compatibility.h
%{_includedir}/asterisk/format_cap.h
%{_includedir}/asterisk/frame.h
%{_includedir}/asterisk/framehook.h
%{_includedir}/asterisk/fskmodem.h
%{_includedir}/asterisk/fskmodem_float.h
%{_includedir}/asterisk/fskmodem_int.h
%{_includedir}/asterisk/global_datastores.h
%{_includedir}/asterisk/hashtab.h
%{_includedir}/asterisk/heap.h
%{_includedir}/asterisk/http.h
%{_includedir}/asterisk/http_websocket.h
%{_includedir}/asterisk/image.h
%{_includedir}/asterisk/indications.h
%{_includedir}/asterisk/inline_api.h
%{_includedir}/asterisk/io.h
%{_includedir}/asterisk/json.h
%{_includedir}/asterisk/linkedlists.h
%{_includedir}/asterisk/localtime.h
%{_includedir}/asterisk/lock.h
%{_includedir}/asterisk/logger.h
%{_includedir}/asterisk/logger_category.h
%{_includedir}/asterisk/manager.h
%{_includedir}/asterisk/md5.h
%{_includedir}/asterisk/media_index.h
%{_includedir}/asterisk/message.h
%{_includedir}/asterisk/mixmonitor.h
%{_includedir}/asterisk/mod_format.h
%{_includedir}/asterisk/module.h
%{_includedir}/asterisk/monitor.h
%{_includedir}/asterisk/musiconhold.h
%{_includedir}/asterisk/netsock2.h
%{_includedir}/asterisk/network.h
%{_includedir}/asterisk/optional_api.h
%{_includedir}/asterisk/options.h
%{_includedir}/asterisk/opus.h
%{_includedir}/asterisk/parking.h
%{_includedir}/asterisk/paths.h
%{_includedir}/asterisk/pbx.h
%{_includedir}/asterisk/phoneprov.h
%{_includedir}/asterisk/pickup.h
%{_includedir}/asterisk/pktccops.h
%{_includedir}/asterisk/plc.h
%{_includedir}/asterisk/poll-compat.h
%{_includedir}/asterisk/presencestate.h
%{_includedir}/asterisk/privacy.h
%{_includedir}/asterisk/pval.h
%{_includedir}/asterisk/refer.h
%{_includedir}/asterisk/res_aeap.h
%{_includedir}/asterisk/res_aeap_message.h
%{_includedir}/asterisk/res_mwi_external.h
%{_includedir}/asterisk/res_odbc.h
%{_includedir}/asterisk/res_audiosocket.h
%{_includedir}/asterisk/res_fax.h
%{_includedir}/asterisk/res_geolocation.h
%{_includedir}/asterisk/res_hep.h
## installer installs res_pjsip headers even when not building res_pjsip
%{_includedir}/asterisk/res_pjproject.h
%{_includedir}/asterisk/res_pjsip_cli.h
%{_includedir}/asterisk/res_pjsip.h
%{_includedir}/asterisk/res_pjsip_body_generator_types.h
%{_includedir}/asterisk/res_pjsip_outbound_publish.h
%{_includedir}/asterisk/res_pjsip_presence_xml.h
%{_includedir}/asterisk/res_pjsip_pubsub.h
%{_includedir}/asterisk/res_pjsip_session.h
%{_includedir}/asterisk/res_pjsip_session_caps.h
%{_includedir}/asterisk/res_prometheus.h
%{_includedir}/asterisk/res_srtp.h
%{_includedir}/asterisk/res_stir_shaken.h
%{_includedir}/asterisk/rtp_engine.h
%{_includedir}/asterisk/say.h
%{_includedir}/asterisk/sched.h
%{_includedir}/asterisk/sdp_srtp.h
%{_includedir}/asterisk/security_events.h
%{_includedir}/asterisk/security_events_defs.h
%{_includedir}/asterisk/select.h
%{_includedir}/asterisk/sem.h
%{_includedir}/asterisk/sha1.h
%{_includedir}/asterisk/silk.h
%{_includedir}/asterisk/sip_api.h
%{_includedir}/asterisk/slin.h
%{_includedir}/asterisk/slinfactory.h
%{_includedir}/asterisk/smdi.h
%{_includedir}/asterisk/smoother.h
%{_includedir}/asterisk/sorcery.h
%{_includedir}/asterisk/sounds_index.h
%{_includedir}/asterisk/speech.h
%{_includedir}/asterisk/spinlock.h
%{_includedir}/asterisk/srv.h
%{_includedir}/asterisk/stasis_app_device_state.h
%{_includedir}/asterisk/stasis_app.h
%{_includedir}/asterisk/stasis_app_impl.h
%{_includedir}/asterisk/stasis_app_mailbox.h
%{_includedir}/asterisk/stasis_app_playback.h
%{_includedir}/asterisk/stasis_app_recording.h
%{_includedir}/asterisk/stasis_app_snoop.h
%{_includedir}/asterisk/stasis_bridges.h
%{_includedir}/asterisk/stasis_cache_pattern.h
%{_includedir}/asterisk/stasis_channels.h
%{_includedir}/asterisk/stasis_endpoints.h
%{_includedir}/asterisk/stasis.h
%{_includedir}/asterisk/stasis_internal.h
%{_includedir}/asterisk/stasis_message_router.h
%{_includedir}/asterisk/stasis_system.h
%{_includedir}/asterisk/stasis_state.h
%{_includedir}/asterisk/stasis_test.h
%{_includedir}/asterisk/statsd.h
%{_includedir}/asterisk/stringfields.h
%{_includedir}/asterisk/strings.h
%{_includedir}/asterisk/stun.h
%{_includedir}/asterisk/syslog.h
%{_includedir}/asterisk/taskprocessor.h
%{_includedir}/asterisk/tcptls.h
%{_includedir}/asterisk/tdd.h
%{_includedir}/asterisk/term.h
%{_includedir}/asterisk/test.h
%{_includedir}/asterisk/threadpool.h
%{_includedir}/asterisk/threadstorage.h
%{_includedir}/asterisk/time.h
%{_includedir}/asterisk/timing.h
%{_includedir}/asterisk/transcap.h
%{_includedir}/asterisk/translate.h
%{_includedir}/asterisk/udptl.h
%{_includedir}/asterisk/ulaw.h
%{_includedir}/asterisk/unaligned.h
%{_includedir}/asterisk/uri.h
%{_includedir}/asterisk/utils.h
%{_includedir}/asterisk/uuid.h
%{_includedir}/asterisk/vector.h
%{_includedir}/asterisk/version.h
%{_includedir}/asterisk/xml.h
%{_includedir}/asterisk/xmldoc.h
%{_includedir}/asterisk/xmpp.h
%{_includedir}/asterisk/doxygen/architecture.h
%{_includedir}/asterisk/doxygen/licensing.h
%{_includedir}/asterisk/alertpipe.h
%{_includedir}/asterisk/dns_core.h
%{_includedir}/asterisk/dns_internal.h
%{_includedir}/asterisk/dns_naptr.h
%{_includedir}/asterisk/dns_query_set.h
%{_includedir}/asterisk/dns_recurring.h
%{_includedir}/asterisk/dns_resolver.h
%{_includedir}/asterisk/dns_srv.h
%{_includedir}/asterisk/dns_test.h
%{_includedir}/asterisk/dns_tlsa.h
%{_includedir}/asterisk/ilbc.h
%{_includedir}/asterisk/max_forwards.h
%{_includedir}/asterisk/media_cache.h
%{_includedir}/asterisk/multicast_rtp.h
%{_includedir}/asterisk/named_locks.h
%{_includedir}/asterisk/res_odbc_transaction.h
%{_includedir}/asterisk/conversions.h
%{_includedir}/asterisk/data_buffer.h
%{_includedir}/asterisk/iostream.h
%{_includedir}/asterisk/mwi.h
%{_includedir}/asterisk/serializer.h
%{_includedir}/asterisk/stream.h
%{_includedir}/asterisk/dns_txt.h
%{_includedir}/asterisk/utf8.h

#
#  Documentation Subpackage
#
%files doc
%defattr(-, root, root)

#
#  Manual Pages
#
%{_mandir}/man8/asterisk.8.gz
%{_mandir}/man8/astgenkey.8.gz
%{_mandir}/man8/autosupport.8.gz
%{_mandir}/man8/safe_asterisk.8.gz
%{_mandir}/man8/astdb2bdb.8.gz
%{_mandir}/man8/astdb2sqlite3.8.gz

#
#  Ogg-Vorbis Subpackage
#
%{?_without_ogg:%if 0}
%{!?_without_ogg:%if 1}
%files ogg
%defattr(-, root, root)
%{_libdir}/asterisk/modules/format_ogg_vorbis.so
%endif

#
#  Speex Subpackage
#
%if %{with speex}
%files speex
%defattr(-, root, root)
%{_libdir}/asterisk/modules/codec_speex.so
%{_libdir}/asterisk/modules/func_speex.so
%{_libdir}/asterisk/modules/format_ogg_speex.so
%endif

#
#  resample Subpackage
#
%{?_without_resample:%if 0}
%{!?_without_resample:%if 1}
%files resample
%defattr(-, root, root)
%{_libdir}/asterisk/modules/codec_resample.so
%endif

#
#  unixODBC Subpackage
#
%if %{with odbc}
%files odbc
%defattr(-, root, root)
%{_libdir}/asterisk/modules/cdr_adaptive_odbc.so
%{_libdir}/asterisk/modules/cdr_odbc.so
%{_libdir}/asterisk/modules/cel_odbc.so
%{_libdir}/asterisk/modules/func_odbc.so
%{_libdir}/asterisk/modules/res_config_odbc.so
%{_libdir}/asterisk/modules/res_odbc.so
%{_libdir}/asterisk/modules/res_odbc_transaction.so
%endif

#
#  voicemail file storage Subpackage
#
%files voicemail
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_voicemail.so

#
#  voicemail ODBC storage Subpackage
#
%{?_without_voicemail_odbcstorage:%if 0}
%{!?_without_voicemail_odbcstorage:%if 1}
%files voicemail-odbcstorage
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_voicemail_odbcstorage.so
%endif

#
#  voicemail IMAP storage Subpackage
#
%{?_without_voicemail_imapstorage:%if 0}
%{!?_without_voicemail_imapstorage:%if 1}
%files voicemail-imapstorage
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_voicemail_imapstorage.so
%endif

%files addons
%defattr(-, root, root)

%files addons-core
%defattr(-, root, root)
%{_libdir}/asterisk/modules/format_mp3.so

%if %{with mysql}
%files addons-mysql
%{_libdir}/asterisk/modules/app_mysql.so
%{_libdir}/asterisk/modules/cdr_mysql.so
%{_libdir}/asterisk/modules/res_config_mysql.so
%endif

%if %{with bluetooth}
%files addons-bluetooth
%{_libdir}/asterisk/modules/chan_mobile.so
%endif

%if %{with ooh323}
%files addons-ooh323
%{_libdir}/asterisk/modules/chan_ooh323.so
%endif

%if %{with opus}
%files opus-opensource
%{_libdir}/asterisk/modules/res_format_attr_vp8.so
%{_libdir}/asterisk/modules/res_format_attr_opus.so
%{_libdir}/asterisk/modules/codec_opus_open_source.so
%endif

%if %{with digium_opus}
%files opus
%{_libdir}/asterisk/modules/format_ogg_opus.so
%{_libdir}/asterisk/modules/codec_opus.so
%{_libdir}/asterisk/modules/res_format_attr_opus.so
%endif


%changelog
* Thu Mar 16 2023 Oded Arbel <odeda@cloudonix.io> - 18.17.0-1
- Stop building res_xmpp so we can remove the dependency on the obsolete and not supported iksemel package
- Fixed radiusclient-ng dependency to not trigger when not building with radius
- Prepare for RHEL 9 builds

* Wed May 05 2021 Oded Arbel <odeda@cloudonix.io> - 18.3.0-2
- Patch 6: ARI get_subscriptions should be able to return NULL if no subscription matched

* Wed Nov 27 2019 Nir Simionovich <nirs@cloudonix.io> - 14.7.8-2
- Add support for Opus complexity patch

* Mon Aug 05 2019 Oded Arbel <odeda@cloudonix.io> - 14.7.8-1
- Add Digium Opus option

* Mon Aug 27 2018 Oded Arbel <odeda@cloudonix.io> - 14.7.2-2
- Remove Opus patch

* Wed Apr 08 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.3.2-1
- Update to 13.3.2.

* Mon Apr 06 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.3.1-1
- Update to 13.3.1.

* Wed Apr 01 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.3.0-1
- Update to 13.3.0.

* Mon Feb 09 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.2.0-1
- Update to 13.2.0.

* Tue Dec 16 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.1.0-1
- Update to 13.1.0.

* Wed Dec 10 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.0.2-1
- Update to 13.0.2.

* Thu Nov 20 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.0.1-1
- Update to 13.0.1.

* Mon Nov 10 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.0.0-1
- Initial 13 build.
