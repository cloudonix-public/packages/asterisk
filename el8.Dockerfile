FROM quay.io/centos/centos:stream8

RUN yum install -y rpm-build rpmdevtools yum-utils epel-release wget
RUN yum config-manager --enable epel powertools
RUN yum install -y opus-devel spandsp-devel make
WORKDIR /root/

# build Asterisk
ADD patches/* /root/rpmbuild/SOURCES/
ADD asterisk.spec /root/rpmbuild/SPECS/asterisk.spec
ADD asterisk.logrotate /root/rpmbuild/SOURCES/
RUN spectool -g -R /root/rpmbuild/SPECS/asterisk.spec --define 'with_opus 1'
RUN yum-builddep -y /root/rpmbuild/SPECS/asterisk.spec --define 'with_opus 1'
RUN rpmbuild -ba rpmbuild/SPECS/asterisk.spec --with opus --without ooh323 --without odbc --without pgsql
