FROM centos:7

WORKDIR /root/
RUN yum install -y rpm-build rpmdevtools yum-utils epel-release wget
ADD asterisk-sounds-*.spec /root/rpmbuild/SPECS/
RUN mkdir -p /root/rpmbuild/SOURCES
RUN spectool -g -R /root/rpmbuild/SPECS/asterisk-sounds-core.spec
RUN yum-builddep -y /root/rpmbuild/SPECS/asterisk-sounds-core.spec
RUN rpmbuild -ba /root/rpmbuild/SPECS/asterisk-sounds-core.spec
