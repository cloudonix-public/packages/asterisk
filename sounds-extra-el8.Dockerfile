FROM quay.io/centos/centos:stream8

WORKDIR /root/
RUN yum install -y rpm-build rpmdevtools yum-utils epel-release wget
ADD asterisk-sounds-*.spec /root/rpmbuild/SPECS/
RUN mkdir -p /root/rpmbuild/SOURCES
RUN spectool -g -R /root/rpmbuild/SPECS/asterisk-sounds-extra.spec
RUN yum-builddep -y /root/rpmbuild/SPECS/asterisk-sounds-extra.spec
RUN rpmbuild -ba /root/rpmbuild/SPECS/asterisk-sounds-extra.spec 2> build.log || cat build.log
